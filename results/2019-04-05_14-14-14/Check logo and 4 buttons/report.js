$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("features/corpHome-test/NavigateNav.feature");
formatter.feature({
  "line": 1,
  "name": "Verify that user can go to all links",
  "description": "",
  "id": "verify-that-user-can-go-to-all-links",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 3,
  "name": "Navigate to relevant sites:[\u003cUsingData\u003e]",
  "description": "",
  "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e]",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "user is on the Scholastic Corporate homepage",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "user clicks on the \u0027navButton\u0027 button",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "user should be brought to the \u0027pageTitle\u0027 expected webpage",
  "keyword": "Then "
});
formatter.examples({
  "line": 8,
  "name": "",
  "description": "",
  "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e];",
  "rows": [
    {
      "cells": [
        "UsingData"
      ],
      "line": 9,
      "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e];;1"
    },
    {
      "cells": [
        "Book_Clubs"
      ],
      "line": 10,
      "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e];;2"
    },
    {
      "cells": [
        "Book_Fairs"
      ],
      "line": 11,
      "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e];;3"
    },
    {
      "cells": [
        "Educators"
      ],
      "line": 12,
      "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e];;4"
    },
    {
      "cells": [
        "Magazines"
      ],
      "line": 13,
      "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e];;5"
    },
    {
      "cells": [
        "Corp_Home"
      ],
      "line": 14,
      "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e];;6"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 4665892006,
  "status": "passed"
});
formatter.scenario({
  "line": 10,
  "name": "Navigate to relevant sites:[Book_Clubs]",
  "description": "",
  "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e];;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "user is on the Scholastic Corporate homepage",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "user clicks on the \u0027navButton\u0027 button",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "user should be brought to the \u0027pageTitle\u0027 expected webpage",
  "keyword": "Then "
});
formatter.match({
  "location": "CorpHomePageSteps.userLaunchApp()"
});
formatter.result({
  "duration": 1274383858,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_clicks_on_button()"
});
formatter.result({
  "duration": 2602508259,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_brought_to_expected_webpage()"
});
formatter.result({
  "duration": 169810587,
  "status": "passed"
});
formatter.after({
  "duration": 1500952978,
  "status": "passed"
});
formatter.before({
  "duration": 3899598340,
  "status": "passed"
});
formatter.scenario({
  "line": 11,
  "name": "Navigate to relevant sites:[Book_Fairs]",
  "description": "",
  "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e];;3",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "user is on the Scholastic Corporate homepage",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "user clicks on the \u0027navButton\u0027 button",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "user should be brought to the \u0027pageTitle\u0027 expected webpage",
  "keyword": "Then "
});
formatter.match({
  "location": "CorpHomePageSteps.userLaunchApp()"
});
formatter.result({
  "duration": 1059616659,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_clicks_on_button()"
});
formatter.result({
  "duration": 3680990601,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_brought_to_expected_webpage()"
});
formatter.result({
  "duration": 363637100,
  "status": "passed"
});
formatter.after({
  "duration": 1321016892,
  "status": "passed"
});
formatter.before({
  "duration": 3939198396,
  "status": "passed"
});
formatter.scenario({
  "line": 12,
  "name": "Navigate to relevant sites:[Educators]",
  "description": "",
  "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e];;4",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "user is on the Scholastic Corporate homepage",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "user clicks on the \u0027navButton\u0027 button",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "user should be brought to the \u0027pageTitle\u0027 expected webpage",
  "keyword": "Then "
});
formatter.match({
  "location": "CorpHomePageSteps.userLaunchApp()"
});
formatter.result({
  "duration": 968032954,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_clicks_on_button()"
});
formatter.result({
  "duration": 4130091712,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_brought_to_expected_webpage()"
});
formatter.result({
  "duration": 15957329,
  "status": "passed"
});
formatter.after({
  "duration": 1300043801,
  "status": "passed"
});
formatter.before({
  "duration": 3771154964,
  "status": "passed"
});
formatter.scenario({
  "line": 13,
  "name": "Navigate to relevant sites:[Magazines]",
  "description": "",
  "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e];;5",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "user is on the Scholastic Corporate homepage",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "user clicks on the \u0027navButton\u0027 button",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "user should be brought to the \u0027pageTitle\u0027 expected webpage",
  "keyword": "Then "
});
formatter.match({
  "location": "CorpHomePageSteps.userLaunchApp()"
});
formatter.result({
  "duration": 949372504,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_clicks_on_button()"
});
formatter.result({
  "duration": 3638719287,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_brought_to_expected_webpage()"
});
formatter.result({
  "duration": 13679794,
  "status": "passed"
});
formatter.after({
  "duration": 1309796642,
  "status": "passed"
});
formatter.before({
  "duration": 3776285765,
  "status": "passed"
});
formatter.scenario({
  "line": 14,
  "name": "Navigate to relevant sites:[Corp_Home]",
  "description": "",
  "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e];;6",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "user is on the Scholastic Corporate homepage",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "user clicks on the \u0027navButton\u0027 button",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "user should be brought to the \u0027pageTitle\u0027 expected webpage",
  "keyword": "Then "
});
formatter.match({
  "location": "CorpHomePageSteps.userLaunchApp()"
});
formatter.result({
  "duration": 1000734514,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_clicks_on_button()"
});
formatter.result({
  "duration": 1512525364,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_brought_to_expected_webpage()"
});
formatter.result({
  "duration": 404354591,
  "status": "passed"
});
formatter.after({
  "duration": 1248909705,
  "status": "passed"
});
});