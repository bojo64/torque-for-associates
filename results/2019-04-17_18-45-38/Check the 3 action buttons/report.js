$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("features/corpHome-test/DontMissOut.feature");
formatter.feature({
  "line": 1,
  "name": "User will be able directed to the specified page when clicking on the the \"Action Link\" button in each card",
  "description": "",
  "id": "user-will-be-able-directed-to-the-specified-page-when-clicking-on-the-the-\"action-link\"-button-in-each-card",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 3,
  "name": "Navigate to the each card\u0027s action link page:[\u003cUsingData\u003e]",
  "description": "",
  "id": "user-will-be-able-directed-to-the-specified-page-when-clicking-on-the-the-\"action-link\"-button-in-each-card;navigate-to-the-each-card\u0027s-action-link-page:[\u003cusingdata\u003e]",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "user is on the Scholastic Corporate Homepage",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "user clicks on the \u0027specifiedButton\u0027 button",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "user should be brought to the expected \u0027pageTitle\u0027",
  "keyword": "Then "
});
formatter.examples({
  "line": 8,
  "name": "",
  "description": "",
  "id": "user-will-be-able-directed-to-the-specified-page-when-clicking-on-the-the-\"action-link\"-button-in-each-card;navigate-to-the-each-card\u0027s-action-link-page:[\u003cusingdata\u003e];",
  "rows": [
    {
      "cells": [
        "UsingData"
      ],
      "line": 9,
      "id": "user-will-be-able-directed-to-the-specified-page-when-clicking-on-the-the-\"action-link\"-button-in-each-card;navigate-to-the-each-card\u0027s-action-link-page:[\u003cusingdata\u003e];;1"
    },
    {
      "cells": [
        "Action1"
      ],
      "line": 10,
      "id": "user-will-be-able-directed-to-the-specified-page-when-clicking-on-the-the-\"action-link\"-button-in-each-card;navigate-to-the-each-card\u0027s-action-link-page:[\u003cusingdata\u003e];;2"
    },
    {
      "cells": [
        "Action2"
      ],
      "line": 11,
      "id": "user-will-be-able-directed-to-the-specified-page-when-clicking-on-the-the-\"action-link\"-button-in-each-card;navigate-to-the-each-card\u0027s-action-link-page:[\u003cusingdata\u003e];;3"
    },
    {
      "cells": [
        "Action3"
      ],
      "line": 12,
      "id": "user-will-be-able-directed-to-the-specified-page-when-clicking-on-the-the-\"action-link\"-button-in-each-card;navigate-to-the-each-card\u0027s-action-link-page:[\u003cusingdata\u003e];;4"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 5360636348,
  "status": "passed"
});
formatter.scenario({
  "line": 10,
  "name": "Navigate to the each card\u0027s action link page:[Action1]",
  "description": "",
  "id": "user-will-be-able-directed-to-the-specified-page-when-clicking-on-the-the-\"action-link\"-button-in-each-card;navigate-to-the-each-card\u0027s-action-link-page:[\u003cusingdata\u003e];;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "user is on the Scholastic Corporate Homepage",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "user clicks on the \u0027specifiedButton\u0027 button",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "user should be brought to the expected \u0027pageTitle\u0027",
  "keyword": "Then "
});
formatter.match({
  "location": "DontMissOutActionSteps.userLaunchApp()"
});
formatter.result({
  "duration": 1065056282,
  "status": "passed"
});
formatter.match({
  "location": "DontMissOutActionSteps.user_clicks_on_button()"
});
formatter.result({
  "duration": 1969814201,
  "status": "passed"
});
formatter.match({
  "location": "DontMissOutActionSteps.user_brought_to_expected_webpage()"
});
formatter.result({
  "duration": 22196886,
  "status": "passed"
});
formatter.after({
  "duration": 1281288920,
  "status": "passed"
});
formatter.before({
  "duration": 3624912101,
  "status": "passed"
});
formatter.scenario({
  "line": 11,
  "name": "Navigate to the each card\u0027s action link page:[Action2]",
  "description": "",
  "id": "user-will-be-able-directed-to-the-specified-page-when-clicking-on-the-the-\"action-link\"-button-in-each-card;navigate-to-the-each-card\u0027s-action-link-page:[\u003cusingdata\u003e];;3",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "user is on the Scholastic Corporate Homepage",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "user clicks on the \u0027specifiedButton\u0027 button",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "user should be brought to the expected \u0027pageTitle\u0027",
  "keyword": "Then "
});
formatter.match({
  "location": "DontMissOutActionSteps.userLaunchApp()"
});
formatter.result({
  "duration": 555093364,
  "status": "passed"
});
formatter.match({
  "location": "DontMissOutActionSteps.user_clicks_on_button()"
});
formatter.result({
  "duration": 1186842381,
  "status": "passed"
});
formatter.match({
  "location": "DontMissOutActionSteps.user_brought_to_expected_webpage()"
});
formatter.result({
  "duration": 8210347,
  "status": "passed"
});
formatter.after({
  "duration": 1381843261,
  "status": "passed"
});
formatter.before({
  "duration": 3693812697,
  "status": "passed"
});
formatter.scenario({
  "line": 12,
  "name": "Navigate to the each card\u0027s action link page:[Action3]",
  "description": "",
  "id": "user-will-be-able-directed-to-the-specified-page-when-clicking-on-the-the-\"action-link\"-button-in-each-card;navigate-to-the-each-card\u0027s-action-link-page:[\u003cusingdata\u003e];;4",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "user is on the Scholastic Corporate Homepage",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "user clicks on the \u0027specifiedButton\u0027 button",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "user should be brought to the expected \u0027pageTitle\u0027",
  "keyword": "Then "
});
formatter.match({
  "location": "DontMissOutActionSteps.userLaunchApp()"
});
formatter.result({
  "duration": 656495636,
  "status": "passed"
});
formatter.match({
  "location": "DontMissOutActionSteps.user_clicks_on_button()"
});
formatter.result({
  "duration": 4550770537,
  "status": "passed"
});
formatter.match({
  "location": "DontMissOutActionSteps.user_brought_to_expected_webpage()"
});
formatter.result({
  "duration": 22603029,
  "status": "passed"
});
formatter.after({
  "duration": 1022647316,
  "status": "passed"
});
});