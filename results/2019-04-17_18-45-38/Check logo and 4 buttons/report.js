$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("features/corpHome-test/NavigateNav.feature");
formatter.feature({
  "line": 1,
  "name": "Verify that user can go to all links",
  "description": "",
  "id": "verify-that-user-can-go-to-all-links",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 3,
  "name": "Navigate to relevant sites:[\u003cUsingData\u003e]",
  "description": "",
  "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e]",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "user is on the Scholastic Corporate homepage",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "user clicks on the \u0027navButton\u0027 button",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "user should be brought to the \u0027pageTitle\u0027 expected webpage",
  "keyword": "Then "
});
formatter.examples({
  "line": 8,
  "name": "",
  "description": "",
  "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e];",
  "rows": [
    {
      "cells": [
        "UsingData"
      ],
      "line": 9,
      "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e];;1"
    },
    {
      "cells": [
        "Book_Clubs"
      ],
      "line": 10,
      "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e];;2"
    },
    {
      "cells": [
        "Book_Fairs"
      ],
      "line": 11,
      "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e];;3"
    },
    {
      "cells": [
        "Educators"
      ],
      "line": 12,
      "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e];;4"
    },
    {
      "cells": [
        "Magazines"
      ],
      "line": 13,
      "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e];;5"
    },
    {
      "cells": [
        "Corp_Home"
      ],
      "line": 14,
      "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e];;6"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 6743490646,
  "status": "passed"
});
formatter.scenario({
  "line": 10,
  "name": "Navigate to relevant sites:[Book_Clubs]",
  "description": "",
  "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e];;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "user is on the Scholastic Corporate homepage",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "user clicks on the \u0027navButton\u0027 button",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "user should be brought to the \u0027pageTitle\u0027 expected webpage",
  "keyword": "Then "
});
formatter.match({
  "location": "CorpHomePageSteps.userLaunchApp()"
});
formatter.result({
  "duration": 1415815045,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_clicks_on_button()"
});
formatter.result({
  "duration": 3532127399,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_brought_to_expected_webpage()"
});
formatter.result({
  "duration": 69061000,
  "status": "passed"
});
formatter.after({
  "duration": 1567298708,
  "status": "passed"
});
formatter.before({
  "duration": 3829625841,
  "status": "passed"
});
formatter.scenario({
  "line": 11,
  "name": "Navigate to relevant sites:[Book_Fairs]",
  "description": "",
  "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e];;3",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "user is on the Scholastic Corporate homepage",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "user clicks on the \u0027navButton\u0027 button",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "user should be brought to the \u0027pageTitle\u0027 expected webpage",
  "keyword": "Then "
});
formatter.match({
  "location": "CorpHomePageSteps.userLaunchApp()"
});
formatter.result({
  "duration": 767224609,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_clicks_on_button()"
});
formatter.result({
  "duration": 7225917413,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_brought_to_expected_webpage()"
});
formatter.result({
  "duration": 432817950,
  "status": "passed"
});
formatter.after({
  "duration": 1442633425,
  "status": "passed"
});
formatter.before({
  "duration": 3866126296,
  "status": "passed"
});
formatter.scenario({
  "line": 12,
  "name": "Navigate to relevant sites:[Educators]",
  "description": "",
  "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e];;4",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "user is on the Scholastic Corporate homepage",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "user clicks on the \u0027navButton\u0027 button",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "user should be brought to the \u0027pageTitle\u0027 expected webpage",
  "keyword": "Then "
});
formatter.match({
  "location": "CorpHomePageSteps.userLaunchApp()"
});
formatter.result({
  "duration": 689905734,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_clicks_on_button()"
});
formatter.result({
  "duration": 4338407056,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_brought_to_expected_webpage()"
});
formatter.result({
  "duration": 21256601,
  "status": "passed"
});
formatter.after({
  "duration": 1277035763,
  "status": "passed"
});
formatter.before({
  "duration": 3757133151,
  "status": "passed"
});
formatter.scenario({
  "line": 13,
  "name": "Navigate to relevant sites:[Magazines]",
  "description": "",
  "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e];;5",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "user is on the Scholastic Corporate homepage",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "user clicks on the \u0027navButton\u0027 button",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "user should be brought to the \u0027pageTitle\u0027 expected webpage",
  "keyword": "Then "
});
formatter.match({
  "location": "CorpHomePageSteps.userLaunchApp()"
});
formatter.result({
  "duration": 596410282,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_clicks_on_button()"
});
formatter.result({
  "duration": 5014512580,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_brought_to_expected_webpage()"
});
formatter.result({
  "duration": 36177485,
  "status": "passed"
});
formatter.after({
  "duration": 1687717316,
  "status": "passed"
});
formatter.before({
  "duration": 4543303345,
  "status": "passed"
});
formatter.scenario({
  "line": 14,
  "name": "Navigate to relevant sites:[Corp_Home]",
  "description": "",
  "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e];;6",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "user is on the Scholastic Corporate homepage",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "user clicks on the \u0027navButton\u0027 button",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "user should be brought to the \u0027pageTitle\u0027 expected webpage",
  "keyword": "Then "
});
formatter.match({
  "location": "CorpHomePageSteps.userLaunchApp()"
});
formatter.result({
  "duration": 672499387,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_clicks_on_button()"
});
formatter.result({
  "duration": 2417459599,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_brought_to_expected_webpage()"
});
formatter.result({
  "duration": 23268412,
  "status": "passed"
});
formatter.after({
  "duration": 1439248183,
  "status": "passed"
});
});