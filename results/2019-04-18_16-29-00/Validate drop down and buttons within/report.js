$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("features/corpHome-test/NavigateNav.feature");
formatter.feature({
  "line": 1,
  "name": "Verify that user can go to all links",
  "description": "",
  "id": "verify-that-user-can-go-to-all-links",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 16,
  "name": "Navigate to linked stores:[\u003cUsingData\u003e]",
  "description": "",
  "id": "verify-that-user-can-go-to-all-links;navigate-to-linked-stores:[\u003cusingdata\u003e]",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 17,
  "name": "user is on the Scholastic Corporate homepage",
  "keyword": "Given "
});
formatter.step({
  "line": 18,
  "name": "user hovers over the Shop dropdown",
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "user clicks on the \u0027navButton\u0027 button",
  "keyword": "When "
});
formatter.step({
  "line": 20,
  "name": "user should be brought to the \u0027pageTitle\u0027 expected webpage",
  "keyword": "Then "
});
formatter.examples({
  "line": 22,
  "name": "",
  "description": "",
  "id": "verify-that-user-can-go-to-all-links;navigate-to-linked-stores:[\u003cusingdata\u003e];",
  "rows": [
    {
      "cells": [
        "UsingData"
      ],
      "line": 23,
      "id": "verify-that-user-can-go-to-all-links;navigate-to-linked-stores:[\u003cusingdata\u003e];;1"
    },
    {
      "cells": [
        "The_Teacher_Store"
      ],
      "line": 24,
      "id": "verify-that-user-can-go-to-all-links;navigate-to-linked-stores:[\u003cusingdata\u003e];;2"
    },
    {
      "cells": [
        "The_Scholastic_Store"
      ],
      "line": 25,
      "id": "verify-that-user-can-go-to-all-links;navigate-to-linked-stores:[\u003cusingdata\u003e];;3"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 3735891132,
  "status": "passed"
});
formatter.scenario({
  "line": 24,
  "name": "Navigate to linked stores:[The_Teacher_Store]",
  "description": "",
  "id": "verify-that-user-can-go-to-all-links;navigate-to-linked-stores:[\u003cusingdata\u003e];;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 17,
  "name": "user is on the Scholastic Corporate homepage",
  "keyword": "Given "
});
formatter.step({
  "line": 18,
  "name": "user hovers over the Shop dropdown",
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "user clicks on the \u0027navButton\u0027 button",
  "keyword": "When "
});
formatter.step({
  "line": 20,
  "name": "user should be brought to the \u0027pageTitle\u0027 expected webpage",
  "keyword": "Then "
});
formatter.match({
  "location": "CorpHomePageSteps.userLaunchApp()"
});
formatter.result({
  "duration": 493854391,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_hovers_over_dropdown()"
});
formatter.result({
  "duration": 76515231,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_clicks_on_button()"
});
formatter.result({
  "duration": 2991133991,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_brought_to_expected_webpage()"
});
formatter.result({
  "duration": 78609134,
  "status": "passed"
});
formatter.after({
  "duration": 1411298328,
  "status": "passed"
});
formatter.before({
  "duration": 3708733580,
  "status": "passed"
});
formatter.scenario({
  "line": 25,
  "name": "Navigate to linked stores:[The_Scholastic_Store]",
  "description": "",
  "id": "verify-that-user-can-go-to-all-links;navigate-to-linked-stores:[\u003cusingdata\u003e];;3",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 17,
  "name": "user is on the Scholastic Corporate homepage",
  "keyword": "Given "
});
formatter.step({
  "line": 18,
  "name": "user hovers over the Shop dropdown",
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "user clicks on the \u0027navButton\u0027 button",
  "keyword": "When "
});
formatter.step({
  "line": 20,
  "name": "user should be brought to the \u0027pageTitle\u0027 expected webpage",
  "keyword": "Then "
});
formatter.match({
  "location": "CorpHomePageSteps.userLaunchApp()"
});
formatter.result({
  "duration": 640889928,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_hovers_over_dropdown()"
});
formatter.result({
  "duration": 224279880,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_clicks_on_button()"
});
formatter.result({
  "duration": 3967652247,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_brought_to_expected_webpage()"
});
formatter.result({
  "duration": 74007083,
  "status": "passed"
});
formatter.after({
  "duration": 1333486897,
  "status": "passed"
});
});