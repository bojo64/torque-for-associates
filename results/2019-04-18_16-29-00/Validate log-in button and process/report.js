$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("features/corpHome-test/NavigateNav.feature");
formatter.feature({
  "line": 1,
  "name": "Verify that user can go to all links",
  "description": "",
  "id": "verify-that-user-can-go-to-all-links",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 27,
  "name": "Navigate to log-in page and log-in:[\u003cUsingData\u003e]",
  "description": "",
  "id": "verify-that-user-can-go-to-all-links;navigate-to-log-in-page-and-log-in:[\u003cusingdata\u003e]",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 28,
  "name": "user is on the Scholastic Corporate homepage",
  "keyword": "Given "
});
formatter.step({
  "line": 29,
  "name": "user clicks on the \u0027navButton\u0027 button",
  "keyword": "When "
});
formatter.step({
  "line": 30,
  "name": "user should be brought to the \u0027pageTitle\u0027 expected webpage",
  "keyword": "Then "
});
formatter.step({
  "line": 31,
  "name": "user enters their \u0027emailCredential\u0027 and \u0027passwordCredential\u0027 and sign-in",
  "keyword": "When "
});
formatter.step({
  "line": 32,
  "name": "user should be brought back to the \u0027corporateHomeTitle\u0027",
  "keyword": "Then "
});
formatter.examples({
  "line": 34,
  "name": "",
  "description": "",
  "id": "verify-that-user-can-go-to-all-links;navigate-to-log-in-page-and-log-in:[\u003cusingdata\u003e];",
  "rows": [
    {
      "cells": [
        "UsingData"
      ],
      "line": 35,
      "id": "verify-that-user-can-go-to-all-links;navigate-to-log-in-page-and-log-in:[\u003cusingdata\u003e];;1"
    },
    {
      "cells": [
        "Complete_Login"
      ],
      "line": 36,
      "id": "verify-that-user-can-go-to-all-links;navigate-to-log-in-page-and-log-in:[\u003cusingdata\u003e];;2"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 3776263984,
  "status": "passed"
});
formatter.scenario({
  "line": 36,
  "name": "Navigate to log-in page and log-in:[Complete_Login]",
  "description": "",
  "id": "verify-that-user-can-go-to-all-links;navigate-to-log-in-page-and-log-in:[\u003cusingdata\u003e];;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 28,
  "name": "user is on the Scholastic Corporate homepage",
  "keyword": "Given "
});
formatter.step({
  "line": 29,
  "name": "user clicks on the \u0027navButton\u0027 button",
  "keyword": "When "
});
formatter.step({
  "line": 30,
  "name": "user should be brought to the \u0027pageTitle\u0027 expected webpage",
  "keyword": "Then "
});
formatter.step({
  "line": 31,
  "name": "user enters their \u0027emailCredential\u0027 and \u0027passwordCredential\u0027 and sign-in",
  "keyword": "When "
});
formatter.step({
  "line": 32,
  "name": "user should be brought back to the \u0027corporateHomeTitle\u0027",
  "keyword": "Then "
});
formatter.match({
  "location": "CorpHomePageSteps.userLaunchApp()"
});
formatter.result({
  "duration": 602233460,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_clicks_on_button()"
});
formatter.result({
  "duration": 1140072781,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_brought_to_expected_webpage()"
});
formatter.result({
  "duration": 22220650,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_logs_in()"
});
formatter.result({
  "duration": 1516660492,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_brought_to_corporate_home_webpage()"
});
formatter.result({
  "duration": 3063291829,
  "status": "passed"
});
formatter.after({
  "duration": 1216436249,
  "status": "passed"
});
});