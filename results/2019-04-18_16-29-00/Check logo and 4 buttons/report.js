$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("features/corpHome-test/NavigateNav.feature");
formatter.feature({
  "line": 1,
  "name": "Verify that user can go to all links",
  "description": "",
  "id": "verify-that-user-can-go-to-all-links",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 3,
  "name": "Navigate to relevant sites:[\u003cUsingData\u003e]",
  "description": "",
  "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e]",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "user is on the Scholastic Corporate homepage",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "user clicks on the \u0027navButton\u0027 button",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "user should be brought to the \u0027pageTitle\u0027 expected webpage",
  "keyword": "Then "
});
formatter.examples({
  "line": 8,
  "name": "",
  "description": "",
  "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e];",
  "rows": [
    {
      "cells": [
        "UsingData"
      ],
      "line": 9,
      "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e];;1"
    },
    {
      "cells": [
        "Book_Clubs"
      ],
      "line": 10,
      "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e];;2"
    },
    {
      "cells": [
        "Book_Fairs"
      ],
      "line": 11,
      "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e];;3"
    },
    {
      "cells": [
        "Educators"
      ],
      "line": 12,
      "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e];;4"
    },
    {
      "cells": [
        "Magazines"
      ],
      "line": 13,
      "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e];;5"
    },
    {
      "cells": [
        "Corp_Home"
      ],
      "line": 14,
      "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e];;6"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 6657777795,
  "status": "passed"
});
formatter.scenario({
  "line": 10,
  "name": "Navigate to relevant sites:[Book_Clubs]",
  "description": "",
  "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e];;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "user is on the Scholastic Corporate homepage",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "user clicks on the \u0027navButton\u0027 button",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "user should be brought to the \u0027pageTitle\u0027 expected webpage",
  "keyword": "Then "
});
formatter.match({
  "location": "CorpHomePageSteps.userLaunchApp()"
});
formatter.result({
  "duration": 1231217791,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_clicks_on_button()"
});
formatter.result({
  "duration": 3122356967,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_brought_to_expected_webpage()"
});
formatter.result({
  "duration": 101228366,
  "status": "passed"
});
formatter.after({
  "duration": 1395783353,
  "status": "passed"
});
formatter.before({
  "duration": 3825333259,
  "status": "passed"
});
formatter.scenario({
  "line": 11,
  "name": "Navigate to relevant sites:[Book_Fairs]",
  "description": "",
  "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e];;3",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "user is on the Scholastic Corporate homepage",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "user clicks on the \u0027navButton\u0027 button",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "user should be brought to the \u0027pageTitle\u0027 expected webpage",
  "keyword": "Then "
});
formatter.match({
  "location": "CorpHomePageSteps.userLaunchApp()"
});
formatter.result({
  "duration": 1168656154,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_clicks_on_button()"
});
formatter.result({
  "duration": 4199279421,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_brought_to_expected_webpage()"
});
formatter.result({
  "duration": 61128256,
  "status": "passed"
});
formatter.after({
  "duration": 1401641637,
  "status": "passed"
});
formatter.before({
  "duration": 3730368779,
  "status": "passed"
});
formatter.scenario({
  "line": 12,
  "name": "Navigate to relevant sites:[Educators]",
  "description": "",
  "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e];;4",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "user is on the Scholastic Corporate homepage",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "user clicks on the \u0027navButton\u0027 button",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "user should be brought to the \u0027pageTitle\u0027 expected webpage",
  "keyword": "Then "
});
formatter.match({
  "location": "CorpHomePageSteps.userLaunchApp()"
});
formatter.result({
  "duration": 586087669,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_clicks_on_button()"
});
formatter.result({
  "duration": 5394918679,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_brought_to_expected_webpage()"
});
formatter.result({
  "duration": 20228282,
  "status": "passed"
});
formatter.after({
  "duration": 1291942604,
  "status": "passed"
});
formatter.before({
  "duration": 3682057782,
  "status": "passed"
});
formatter.scenario({
  "line": 13,
  "name": "Navigate to relevant sites:[Magazines]",
  "description": "",
  "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e];;5",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "user is on the Scholastic Corporate homepage",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "user clicks on the \u0027navButton\u0027 button",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "user should be brought to the \u0027pageTitle\u0027 expected webpage",
  "keyword": "Then "
});
formatter.match({
  "location": "CorpHomePageSteps.userLaunchApp()"
});
formatter.result({
  "duration": 1123955074,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_clicks_on_button()"
});
formatter.result({
  "duration": 3559212580,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_brought_to_expected_webpage()"
});
formatter.result({
  "duration": 15022959,
  "status": "passed"
});
formatter.after({
  "duration": 1294414026,
  "status": "passed"
});
formatter.before({
  "duration": 3763058406,
  "status": "passed"
});
formatter.scenario({
  "line": 14,
  "name": "Navigate to relevant sites:[Corp_Home]",
  "description": "",
  "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e];;6",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "user is on the Scholastic Corporate homepage",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "user clicks on the \u0027navButton\u0027 button",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "user should be brought to the \u0027pageTitle\u0027 expected webpage",
  "keyword": "Then "
});
formatter.match({
  "location": "CorpHomePageSteps.userLaunchApp()"
});
formatter.result({
  "duration": 507536323,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_clicks_on_button()"
});
formatter.result({
  "duration": 1761473801,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_brought_to_expected_webpage()"
});
formatter.result({
  "duration": 11247237,
  "status": "passed"
});
formatter.after({
  "duration": 1159174450,
  "status": "passed"
});
});