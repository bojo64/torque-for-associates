$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("features/corpHome-test/NavigateNav.feature");
formatter.feature({
  "line": 1,
  "name": "Verify that user can go to all links",
  "description": "",
  "id": "verify-that-user-can-go-to-all-links",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 3,
  "name": "Navigate to relevant sites:[\u003cUsingData\u003e]",
  "description": "",
  "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e]",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "user is on the Scholastic Corporate homepage",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "user clicks on the \u0027navButton\u0027 button",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "user should be brought to the \u0027pageTitle\u0027 expected webpage",
  "keyword": "Then "
});
formatter.examples({
  "line": 8,
  "name": "",
  "description": "",
  "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e];",
  "rows": [
    {
      "cells": [
        "UsingData"
      ],
      "line": 9,
      "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e];;1"
    },
    {
      "cells": [
        "Book_Clubs"
      ],
      "line": 10,
      "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e];;2"
    },
    {
      "cells": [
        "Book_Fairs"
      ],
      "line": 11,
      "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e];;3"
    },
    {
      "cells": [
        "Educators"
      ],
      "line": 12,
      "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e];;4"
    },
    {
      "cells": [
        "Magazines"
      ],
      "line": 13,
      "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e];;5"
    },
    {
      "cells": [
        "Corp_Home"
      ],
      "line": 14,
      "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e];;6"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 3586367935,
  "status": "passed"
});
formatter.scenario({
  "line": 10,
  "name": "Navigate to relevant sites:[Book_Clubs]",
  "description": "",
  "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e];;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "user is on the Scholastic Corporate homepage",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "user clicks on the \u0027navButton\u0027 button",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "user should be brought to the \u0027pageTitle\u0027 expected webpage",
  "keyword": "Then "
});
formatter.match({
  "location": "CorpHomePageSteps.userLaunchApp()"
});
formatter.result({
  "duration": 415113518,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_clicks_on_button()"
});
formatter.result({
  "duration": 2154958438,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_brought_to_expected_webpage()"
});
formatter.result({
  "duration": 84180862,
  "status": "passed"
});
formatter.after({
  "duration": 1074949731,
  "status": "passed"
});
formatter.before({
  "duration": 3549564394,
  "status": "passed"
});
formatter.scenario({
  "line": 11,
  "name": "Navigate to relevant sites:[Book_Fairs]",
  "description": "",
  "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e];;3",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "user is on the Scholastic Corporate homepage",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "user clicks on the \u0027navButton\u0027 button",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "user should be brought to the \u0027pageTitle\u0027 expected webpage",
  "keyword": "Then "
});
formatter.match({
  "location": "CorpHomePageSteps.userLaunchApp()"
});
formatter.result({
  "duration": 428146305,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_clicks_on_button()"
});
formatter.result({
  "duration": 4092408514,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_brought_to_expected_webpage()"
});
formatter.result({
  "duration": 22516676,
  "status": "passed"
});
formatter.after({
  "duration": 1107340779,
  "status": "passed"
});
formatter.before({
  "duration": 3691905370,
  "status": "passed"
});
formatter.scenario({
  "line": 12,
  "name": "Navigate to relevant sites:[Educators]",
  "description": "",
  "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e];;4",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "user is on the Scholastic Corporate homepage",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "user clicks on the \u0027navButton\u0027 button",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "user should be brought to the \u0027pageTitle\u0027 expected webpage",
  "keyword": "Then "
});
formatter.match({
  "location": "CorpHomePageSteps.userLaunchApp()"
});
formatter.result({
  "duration": 480847242,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_clicks_on_button()"
});
formatter.result({
  "duration": 5494403911,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_brought_to_expected_webpage()"
});
formatter.result({
  "duration": 9538438,
  "status": "passed"
});
formatter.after({
  "duration": 1089638959,
  "status": "passed"
});
formatter.before({
  "duration": 3632770943,
  "status": "passed"
});
formatter.scenario({
  "line": 13,
  "name": "Navigate to relevant sites:[Magazines]",
  "description": "",
  "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e];;5",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "user is on the Scholastic Corporate homepage",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "user clicks on the \u0027navButton\u0027 button",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "user should be brought to the \u0027pageTitle\u0027 expected webpage",
  "keyword": "Then "
});
formatter.match({
  "location": "CorpHomePageSteps.userLaunchApp()"
});
formatter.result({
  "duration": 460968339,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_clicks_on_button()"
});
formatter.result({
  "duration": 2917993520,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_brought_to_expected_webpage()"
});
formatter.result({
  "duration": 26149827,
  "status": "passed"
});
formatter.after({
  "duration": 1127281250,
  "status": "passed"
});
formatter.before({
  "duration": 3593181645,
  "status": "passed"
});
formatter.scenario({
  "line": 14,
  "name": "Navigate to relevant sites:[Corp_Home]",
  "description": "",
  "id": "verify-that-user-can-go-to-all-links;navigate-to-relevant-sites:[\u003cusingdata\u003e];;6",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "user is on the Scholastic Corporate homepage",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "user clicks on the \u0027navButton\u0027 button",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "user should be brought to the \u0027pageTitle\u0027 expected webpage",
  "keyword": "Then "
});
formatter.match({
  "location": "CorpHomePageSteps.userLaunchApp()"
});
formatter.result({
  "duration": 439260170,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_clicks_on_button()"
});
formatter.result({
  "duration": 1497634158,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_brought_to_expected_webpage()"
});
formatter.result({
  "duration": 9980228,
  "status": "passed"
});
formatter.after({
  "duration": 1046331176,
  "status": "passed"
});
});