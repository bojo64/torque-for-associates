$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("features/corpHome-test/NavigateNav.feature");
formatter.feature({
  "line": 1,
  "name": "Verify that user can go to all links",
  "description": "",
  "id": "verify-that-user-can-go-to-all-links",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 38,
  "name": "Navigate to log-in page and back to Corporate Home:[\u003cUsingData\u003e]",
  "description": "",
  "id": "verify-that-user-can-go-to-all-links;navigate-to-log-in-page-and-back-to-corporate-home:[\u003cusingdata\u003e]",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 39,
  "name": "user is on the Scholastic Corporate homepage",
  "keyword": "Given "
});
formatter.step({
  "line": 40,
  "name": "user clicks on the \u0027navButton\u0027 button",
  "keyword": "When "
});
formatter.step({
  "line": 41,
  "name": "user should be brought to the \u0027pageTitle\u0027 expected webpage",
  "keyword": "Then "
});
formatter.step({
  "line": 42,
  "name": "user navigates back one page",
  "keyword": "When "
});
formatter.step({
  "line": 43,
  "name": "user should be brought back to the \u0027corporateHomeTitle\u0027",
  "keyword": "Then "
});
formatter.examples({
  "line": 45,
  "name": "",
  "description": "",
  "id": "verify-that-user-can-go-to-all-links;navigate-to-log-in-page-and-back-to-corporate-home:[\u003cusingdata\u003e];",
  "rows": [
    {
      "cells": [
        "UsingData"
      ],
      "line": 46,
      "id": "verify-that-user-can-go-to-all-links;navigate-to-log-in-page-and-back-to-corporate-home:[\u003cusingdata\u003e];;1"
    },
    {
      "cells": [
        "InComplete_Login"
      ],
      "line": 47,
      "id": "verify-that-user-can-go-to-all-links;navigate-to-log-in-page-and-back-to-corporate-home:[\u003cusingdata\u003e];;2"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 3596600382,
  "status": "passed"
});
formatter.scenario({
  "line": 47,
  "name": "Navigate to log-in page and back to Corporate Home:[InComplete_Login]",
  "description": "",
  "id": "verify-that-user-can-go-to-all-links;navigate-to-log-in-page-and-back-to-corporate-home:[\u003cusingdata\u003e];;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 39,
  "name": "user is on the Scholastic Corporate homepage",
  "keyword": "Given "
});
formatter.step({
  "line": 40,
  "name": "user clicks on the \u0027navButton\u0027 button",
  "keyword": "When "
});
formatter.step({
  "line": 41,
  "name": "user should be brought to the \u0027pageTitle\u0027 expected webpage",
  "keyword": "Then "
});
formatter.step({
  "line": 42,
  "name": "user navigates back one page",
  "keyword": "When "
});
formatter.step({
  "line": 43,
  "name": "user should be brought back to the \u0027corporateHomeTitle\u0027",
  "keyword": "Then "
});
formatter.match({
  "location": "CorpHomePageSteps.userLaunchApp()"
});
formatter.result({
  "duration": 1078344165,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_clicks_on_button()"
});
formatter.result({
  "duration": 755555988,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_brought_to_expected_webpage()"
});
formatter.result({
  "duration": 21905840,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_does_not_log_in()"
});
formatter.result({
  "duration": 103404101,
  "status": "passed"
});
formatter.match({
  "location": "CorpHomePageSteps.user_brought_to_corporate_home_webpage()"
});
formatter.result({
  "duration": 3045546433,
  "status": "passed"
});
formatter.after({
  "duration": 907776195,
  "status": "passed"
});
});