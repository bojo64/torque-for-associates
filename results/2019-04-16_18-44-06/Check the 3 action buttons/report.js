$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("features/corpHome-test/DontMissOut.feature");
formatter.feature({
  "line": 1,
  "name": "User will be able directed to the specified page when clicking on the the \"Action Link\" button in each card",
  "description": "",
  "id": "user-will-be-able-directed-to-the-specified-page-when-clicking-on-the-the-\"action-link\"-button-in-each-card",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 3,
  "name": "Navigate to the each card\u0027s action link page:[\u003cUsingData\u003e]",
  "description": "",
  "id": "user-will-be-able-directed-to-the-specified-page-when-clicking-on-the-the-\"action-link\"-button-in-each-card;navigate-to-the-each-card\u0027s-action-link-page:[\u003cusingdata\u003e]",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "user is on the Scholastic Corporate Homepage",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "user clicks on the \u0027specifiedButton\u0027 button",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "user should be brought to the expected \u0027pageTitle\u0027",
  "keyword": "Then "
});
formatter.examples({
  "line": 8,
  "name": "",
  "description": "",
  "id": "user-will-be-able-directed-to-the-specified-page-when-clicking-on-the-the-\"action-link\"-button-in-each-card;navigate-to-the-each-card\u0027s-action-link-page:[\u003cusingdata\u003e];",
  "rows": [
    {
      "cells": [
        "UsingData"
      ],
      "line": 9,
      "id": "user-will-be-able-directed-to-the-specified-page-when-clicking-on-the-the-\"action-link\"-button-in-each-card;navigate-to-the-each-card\u0027s-action-link-page:[\u003cusingdata\u003e];;1"
    },
    {
      "cells": [
        "Action1"
      ],
      "line": 10,
      "id": "user-will-be-able-directed-to-the-specified-page-when-clicking-on-the-the-\"action-link\"-button-in-each-card;navigate-to-the-each-card\u0027s-action-link-page:[\u003cusingdata\u003e];;2"
    },
    {
      "cells": [
        "Action2"
      ],
      "line": 11,
      "id": "user-will-be-able-directed-to-the-specified-page-when-clicking-on-the-the-\"action-link\"-button-in-each-card;navigate-to-the-each-card\u0027s-action-link-page:[\u003cusingdata\u003e];;3"
    },
    {
      "cells": [
        "Action3"
      ],
      "line": 12,
      "id": "user-will-be-able-directed-to-the-specified-page-when-clicking-on-the-the-\"action-link\"-button-in-each-card;navigate-to-the-each-card\u0027s-action-link-page:[\u003cusingdata\u003e];;4"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 4354745537,
  "status": "passed"
});
formatter.scenario({
  "line": 10,
  "name": "Navigate to the each card\u0027s action link page:[Action1]",
  "description": "",
  "id": "user-will-be-able-directed-to-the-specified-page-when-clicking-on-the-the-\"action-link\"-button-in-each-card;navigate-to-the-each-card\u0027s-action-link-page:[\u003cusingdata\u003e];;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "user is on the Scholastic Corporate Homepage",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "user clicks on the \u0027specifiedButton\u0027 button",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "user should be brought to the expected \u0027pageTitle\u0027",
  "keyword": "Then "
});
formatter.match({
  "location": "DontMissOutActionSteps.userLaunchApp()"
});
formatter.result({
  "duration": 729590870,
  "status": "passed"
});
formatter.match({
  "location": "DontMissOutActionSteps.user_clicks_on_button()"
});
formatter.result({
  "duration": 6045408172,
  "status": "passed"
});
formatter.match({
  "location": "DontMissOutActionSteps.user_brought_to_expected_webpage()"
});
formatter.result({
  "duration": 10897292,
  "status": "passed"
});
formatter.after({
  "duration": 1117152500,
  "status": "passed"
});
formatter.before({
  "duration": 3590259786,
  "status": "passed"
});
formatter.scenario({
  "line": 11,
  "name": "Navigate to the each card\u0027s action link page:[Action2]",
  "description": "",
  "id": "user-will-be-able-directed-to-the-specified-page-when-clicking-on-the-the-\"action-link\"-button-in-each-card;navigate-to-the-each-card\u0027s-action-link-page:[\u003cusingdata\u003e];;3",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "user is on the Scholastic Corporate Homepage",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "user clicks on the \u0027specifiedButton\u0027 button",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "user should be brought to the expected \u0027pageTitle\u0027",
  "keyword": "Then "
});
formatter.match({
  "location": "DontMissOutActionSteps.userLaunchApp()"
});
formatter.result({
  "duration": 474418072,
  "status": "passed"
});
formatter.match({
  "location": "DontMissOutActionSteps.user_clicks_on_button()"
});
formatter.result({
  "duration": 963113286,
  "status": "passed"
});
formatter.match({
  "location": "DontMissOutActionSteps.user_brought_to_expected_webpage()"
});
formatter.result({
  "duration": 10234607,
  "status": "passed"
});
formatter.after({
  "duration": 1000296507,
  "status": "passed"
});
formatter.before({
  "duration": 3606816087,
  "status": "passed"
});
formatter.scenario({
  "line": 12,
  "name": "Navigate to the each card\u0027s action link page:[Action3]",
  "description": "",
  "id": "user-will-be-able-directed-to-the-specified-page-when-clicking-on-the-the-\"action-link\"-button-in-each-card;navigate-to-the-each-card\u0027s-action-link-page:[\u003cusingdata\u003e];;4",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "user is on the Scholastic Corporate Homepage",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "user clicks on the \u0027specifiedButton\u0027 button",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "user should be brought to the expected \u0027pageTitle\u0027",
  "keyword": "Then "
});
formatter.match({
  "location": "DontMissOutActionSteps.userLaunchApp()"
});
formatter.result({
  "duration": 430916940,
  "status": "passed"
});
formatter.match({
  "location": "DontMissOutActionSteps.user_clicks_on_button()"
});
formatter.result({
  "duration": 3442141523,
  "status": "passed"
});
formatter.match({
  "location": "DontMissOutActionSteps.user_brought_to_expected_webpage()"
});
formatter.result({
  "duration": 20408185,
  "status": "passed"
});
formatter.after({
  "duration": 1073920870,
  "status": "passed"
});
});