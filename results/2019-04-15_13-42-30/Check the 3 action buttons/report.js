$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("features/corpHome-test/DontMissOut.feature");
formatter.feature({
  "line": 1,
  "name": "User will be able directed to the specified page when clicking on the the \"Action Link\" button in each card",
  "description": "",
  "id": "user-will-be-able-directed-to-the-specified-page-when-clicking-on-the-the-\"action-link\"-button-in-each-card",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 3,
  "name": "Navigate to the each card\u0027s action link page: [\u003cUsingData\u003e]",
  "description": "",
  "id": "user-will-be-able-directed-to-the-specified-page-when-clicking-on-the-the-\"action-link\"-button-in-each-card;navigate-to-the-each-card\u0027s-action-link-page:-[\u003cusingdata\u003e]",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "user is on the Scholastic Corporate Homepage",
  "keyword": "When "
});
formatter.step({
  "line": 5,
  "name": "user clicks on the \u0027specifiedButton\u0027 button",
  "keyword": "And "
});
formatter.step({
  "line": 6,
  "name": "user should be brought to the expected \u0027pageTitle\u0027",
  "keyword": "Then "
});
formatter.examples({
  "line": 8,
  "name": "",
  "description": "",
  "id": "user-will-be-able-directed-to-the-specified-page-when-clicking-on-the-the-\"action-link\"-button-in-each-card;navigate-to-the-each-card\u0027s-action-link-page:-[\u003cusingdata\u003e];",
  "rows": [
    {
      "cells": [
        "UsingData"
      ],
      "line": 9,
      "id": "user-will-be-able-directed-to-the-specified-page-when-clicking-on-the-the-\"action-link\"-button-in-each-card;navigate-to-the-each-card\u0027s-action-link-page:-[\u003cusingdata\u003e];;1"
    },
    {
      "cells": [
        "Action_1"
      ],
      "line": 10,
      "id": "user-will-be-able-directed-to-the-specified-page-when-clicking-on-the-the-\"action-link\"-button-in-each-card;navigate-to-the-each-card\u0027s-action-link-page:-[\u003cusingdata\u003e];;2"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 4193202176,
  "status": "passed"
});
formatter.scenario({
  "line": 10,
  "name": "Navigate to the each card\u0027s action link page: [Action_1]",
  "description": "",
  "id": "user-will-be-able-directed-to-the-specified-page-when-clicking-on-the-the-\"action-link\"-button-in-each-card;navigate-to-the-each-card\u0027s-action-link-page:-[\u003cusingdata\u003e];;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "user is on the Scholastic Corporate Homepage",
  "keyword": "When "
});
formatter.step({
  "line": 5,
  "name": "user clicks on the \u0027specifiedButton\u0027 button",
  "keyword": "And "
});
formatter.step({
  "line": 6,
  "name": "user should be brought to the expected \u0027pageTitle\u0027",
  "keyword": "Then "
});
formatter.match({
  "location": "DontMissOutActionSteps.userLaunchApp()"
});
formatter.result({
  "duration": 729516630,
  "status": "passed"
});
formatter.match({
  "location": "DontMissOutActionSteps.user_clicks_on_button()"
});
formatter.result({
  "duration": 621097,
  "error_message": "java.lang.NullPointerException\r\n\tat com.corpHomeTest.pages.CorpHomePage.clickExpectedButton(CorpHomePage.java:70)\r\n\tat com.corpHomeTest.steps.DontMissOutActionSteps.user_clicks_on_button(DontMissOutActionSteps.java:26)\r\n\tat ✽.And user clicks on the \u0027specifiedButton\u0027 button(features/corpHome-test/DontMissOut.feature:5)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "DontMissOutActionSteps.user_brought_to_expected_webpage()"
});
formatter.result({
  "status": "skipped"
});
formatter.write("Current Page URL is https://shop-qa64.scholastic.com/content/corp-home/babatest/Dont_Miss_Out1.html");
formatter.embedding("image/png", "embedded0.png");
formatter.after({
  "duration": 2155460549,
  "status": "passed"
});
});