/* PLEASE READ
 * - This folder/file and all other locators are used to "store" WebElement paths (or URLs or anything else really) to be used repeatedly in one or multiple tests.
 * - You can reference certain variables made in the Environment Properties (env.properties) file found in the resources folder (e.g., CORP_HOME_PAGE_URL/Scholastic.CorpHome.URL)
 * - To reference a locator in a Page object, use the keyword is "implements" at the class level
 */
package com.corpHomeTest.locators;

public interface CorpHomeLoc {

	public interface TopNavigationBar{
		//URLs
		String CORP_HOME_PAGE_URL = "Scholastic.CorpHome.URL";
		
		//Miscellaneous
		String SHOP_DROP_DOWN_ARROW = "//p[@class='globalnav--shop-text']";
		
		//Sign-In for MyAccounts
		String SIGN_IN_EMAIL = "//input[@placeholder='Email address']";
		String SIGN_IN_PASSWORD = "//input[@placeholder='Password']";
		String SIGN_IN_BUTTON = "//button[@class='mysch-btn btn-nxt signin-btn btn-next-nav mysch-reg-btn sch-btn-short']";
	}
}