/* PLEASE READ
 * - This folder/file will hold the basic functions a user will perform on the page and will be called in the Steps class for an automated test case
 * - @FindBy will look for the locator specified in the referenced interface and will store that value during the run time
 * - It may make life easier to make as many methods as dynamic as possible to suite multiple test cases
 */
package com.corpHomeTest.pages;

import static com.scholastic.torque.common.AssertUtils.assertDisplayed;
import static com.scholastic.torque.common.WaitUtils.waitForDisplayed;
import static com.scholastic.torque.common.WrapperFunctions.click;
import static com.scholastic.torque.common.WrapperFunctions.launchURL;
import static com.scholastic.torque.common.WrapperFunctions.sendKeys;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.corpHomeTest.locators.CorpHomeLoc;
import com.corpHomeTest.locators.CorpHomeLoc.TopNavigationBar;
import com.corpHomeTest.utils.MyScholasticCommonUtil;
import com.scholastic.torque.common.BaseTestPage;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.TestPage;
import com.torque.automation.core.TestDataUtils;

/**
 * @author Babatunde Ojo
 */

public class CorpHomePage extends BaseTestPage<TestPage> implements TopNavigationBar{
	
	MyScholasticCommonUtil mySchlUtil = new MyScholasticCommonUtil();
	
	@FindBy(xpath=SHOP_DROP_DOWN_ARROW)
	WebElement shopDropDownArrow;
	
	@FindBy(xpath=SIGN_IN_EMAIL)
	WebElement signInEmail;
	
	@FindBy(xpath=SIGN_IN_PASSWORD)
	WebElement signInPassword;
	
	@FindBy(xpath=SIGN_IN_BUTTON)
	WebElement signInButton;
	
	public WebElement getDropDown() {
		return shopDropDownArrow;
	}
	
	public WebElement getSIE() {
		return signInEmail;
	}
	
	public WebElement getSIP() {
		return signInPassword;
	}
	
	public WebElement getSIB() {
		return signInButton;
	}

	@Override
	public void openPage() {
		System.out.println("Scholastic Corporate Home");
		getDriver().get(getContext().getString(CORP_HOME_PAGE_URL));
	}
	
	public void clickExpectedButton() {
		System.out.println("Clicking specified button");
		String button = TestBaseProvider.getTestBase().getTestData().getString("specifiedButton");
		System.out.println(button);
		getDriver().findElement(By.xpath(button)).click();
	}
	
	public void hoverDropDown(){
		System.out.println("Click/Hover on dropDown");
		getDriver().findElement(By.xpath(SHOP_DROP_DOWN_ARROW)).click();
	}
	
	public void sendLogInCredentials() {
		System.out.println("Sending LogIn Credentials");
		String email = TestBaseProvider.getTestBase().getTestData().getString("emailCredential");
		String password = TestBaseProvider.getTestBase().getTestData().getString("passwordCredential");
		getDriver().findElement(By.xpath(SIGN_IN_EMAIL)).sendKeys(email);
		getDriver().findElement(By.xpath(SIGN_IN_PASSWORD)).sendKeys(password);
		mySchlUtil.waitForVisibility(signInButton);
		getDriver().findElement(By.xpath(SIGN_IN_BUTTON)).click();
	}
	
	public void verifyPageTitle() {
		System.out.println("Verifying if correct nav button was clicked");
		String title = TestBaseProvider.getTestBase().getTestData().getString("pageTitle");
		Assert.assertTrue(getDriver().getTitle().equalsIgnoreCase(title));
	}
	
	public void verifyCorporateHome() {
		System.out.println("Verify that user is back on Corporate Home Page");
		String corpTitle = TestBaseProvider.getTestBase().getTestData().getString("corporateHomeTitle");
		System.out.println(corpTitle);
		mySchlUtil.pause(3);
		System.out.println(getDriver().getTitle());
		Assert.assertTrue(getDriver().getTitle().equalsIgnoreCase(corpTitle));
	}
	
	

}
