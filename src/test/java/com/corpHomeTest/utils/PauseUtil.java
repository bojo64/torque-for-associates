package com.corpHomeTest.utils;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.scholastic.torque.common.TestBaseProvider;

/**
 * @author Anish ohri
 */

public class PauseUtil {

	public static void waitForAjaxToComplete(long... second) {
		try {
			(new WebDriverWait(TestBaseProvider.getTestBase().getDriver(), 60)).until(new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver d) {
					JavascriptExecutor js = (JavascriptExecutor) TestBaseProvider.getTestBase().getDriver();
					return (Boolean) js.executeScript("return !!window.jQuery && window.jQuery.active == 0");

				}
			});
		} catch (Exception e) {
		}
	}

	public static void pause(long ms) {
		try {
			Thread.sleep(ms);
		} catch (Exception e) {

		}
	}

	public static WebElement getWhenVisible(WebElement element, int timeout) {
		WebDriverWait wait = new WebDriverWait(TestBaseProvider.getTestBase().getDriver(), timeout);
		element = wait.until(ExpectedConditions.visibilityOf(element));
		return element;
	}

	public static void clickWhenReady(WebElement element, int timeout) {
		WebDriverWait wait = new WebDriverWait(TestBaseProvider.getTestBase().getDriver(), timeout);
		element = wait.until(ExpectedConditions.elementToBeClickable(element));
		element.click();
	}
}
