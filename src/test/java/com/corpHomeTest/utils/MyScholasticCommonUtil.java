/* PLEASE READ
 * - This folder/file will hold helpful methods you can use to during your tests (usually in steps, but also pages)
 * - Hosts the CommonUtils you can update/change/add to
 */
package com.corpHomeTest.utils;

import static com.scholastic.torque.common.WrapperFunctions.launchURL;

import java.util.List;
import java.util.Random;

import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.scholastic.torque.common.BaseTestPage;
import com.scholastic.torque.common.LocatorUtils;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.TestPage;
import com.torque.automation.core.TestDataUtils;

public class MyScholasticCommonUtil extends BaseTestPage<TestPage> {

	WebDriverWait wait = null;

	public void pause(long seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (InterruptedException e) {
		}
	}

	public void waitForVisibility(WebElement element) {
		wait = new WebDriverWait(getDriver(), 50);
		wait.until(ExpectedConditions.visibilityOf(element));
	}

	public String getEmail(String email) {
		if (email == null || email.equalsIgnoreCase("")) {
			return "auto_" + RandomStringUtils.randomAlphanumeric(15) + "@asf.com";
		} else {
			return email;
		}
	}

	public String getStringData(String email) {
		if (email == null || email.equalsIgnoreCase("")) {
			return "auto_" + RandomStringUtils.randomAlphabetic(15);
		} else {
			return email;
		}
	}

	public boolean waitAndValidateVisibility(WebElement element) {
		try {
			wait = new WebDriverWait(getDriver(), 50);
			wait.until(ExpectedConditions.visibilityOf(element));
			return true;
		} catch (TimeoutException e) {
			return false;
		}
	}

	public void waitForelementToBeClickable(final WebElement webElement) {
		wait = new WebDriverWait(getDriver(), 120);
		wait.until(ExpectedConditions.elementToBeClickable(webElement));
	}

	public void scrollElementIntoView(WebElement element) {
		((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", element);
	}

	public boolean validateTextInCurrentUrl(String text) {
		return getDriver().getCurrentUrl().contains(text);
	}

	public boolean isElementPresent(By locator) {
		return !getDriver().findElements(locator).isEmpty();
	}

	public boolean isElementPresent(String locatorString) {
		try {
			getDriver().findElement(By.xpath(locatorString));
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public void clickByText(String locator) {
		getDriver().findElement(LocatorUtils.getBy("link='agr'")).click();
	}

	@Override
	public void openPage() {
		// getDriver().get(getContext().getString(HOMEPAGE));
		launchURL(TestDataUtils.getContext().getString("Scholastic.CorpHome.URL"));
	}

	public String getRandomString() {
		return "comment_" + RandomStringUtils.randomAlphabetic(10);
	}

	public void clickOn(WebElement element) {
		Actions actions = new Actions(getDriver());
		actions.contextClick().sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.RETURN).perform();
	}

	public void navigateTo(String page, String subpage) {
		getDriver().get(page + subpage);
	}

	public boolean isElementVisible(WebElement element) {
		return element.isDisplayed();
	}

	public void clickusingJavaScript(WebElement element) {
		JavascriptExecutor executor = (JavascriptExecutor) getDriver();
		executor.executeScript("arguments[0].click();", element);
	}

	public void waitForAjaxToComplete(long... second) {
		try {
			(new WebDriverWait(TestBaseProvider.getTestBase().getDriver(), 60)).until(new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver d) {
					JavascriptExecutor js = (JavascriptExecutor) TestBaseProvider.getTestBase().getDriver();
					return (Boolean) js.executeScript("return !!window.jQuery && window.jQuery.active == 0");

				}
			});
		} catch (Exception e) {
		}
	}

	public void waitForLoaderToDismiss() {
		WebDriverWait wait = new WebDriverWait(TestBaseProvider.getTestBase().getDriver(),
				Integer.parseInt(TestBaseProvider.getTestBase().getContext().getString("wait.timeout.sec")));
		try {
			wait.until(new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver wdriver) {
					return ((JavascriptExecutor) TestBaseProvider.getTestBase().getDriver())
							.executeScript("return document.readyState").equals("complete");
				}
			});
		} catch (Exception e) {
			System.out.println("Waiting for Loader to dismiss timed out");
		}
	}

	public void waitForPageToLoad() {
		waitForAjaxToComplete();
		waitForLoaderToDismiss();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void refreshPage() {
		TestBaseProvider.getTestBase().getDriver().navigate().refresh();
		waitForAjaxToComplete();
		waitForLoaderToDismiss();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void backOnePage() {
		TestBaseProvider.getTestBase().getDriver().navigate().back();
	}

	public String randomWord(int length) {
		Random random = new Random();
		StringBuilder word = new StringBuilder(length);
		for (int i = 0; i < length; i++) {
			word.append((char) ('a' + random.nextInt(26)));
		}

		return word.toString();
	}
	
	public WebElement getComponentByXpath(String componentName) {
		return (WebElement) getDriver().findElement(By.xpath(componentName));
	}
	
	public void verifyLinksArePresent(List<WebElement> elements, String[] linkArray) {		
		for (int i = 0; i < elements.size(); i++) {
			String currTabTitle = elements.get(i).getText();
			Assert.assertTrue(linkArray[i].equalsIgnoreCase(currTabTitle),
					currTabTitle + " tab is out of order or not found");
		}
	}

}
