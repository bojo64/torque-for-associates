package com.corpHomeTest.steps;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;

import com.saucelabs.saucerest.SauceREST;
import com.scholastic.torque.common.SauceUtils;
import com.scholastic.torque.common.ScreenshotUtil;
import com.scholastic.torque.common.TestBase;
import com.scholastic.torque.common.TestBaseProvider;
import com.torque.automation.core.TestDataUtils;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

/**
 * @author Anish ohri
 */

public class Hooks {

	@Before
	public void beforeHook(Scenario scenario) {
		String session;
		TestBase testBase;
		synchronized (this) {

			testBase = TestBaseProvider.getTestBase();

			testBase.getContext().setProperty("scenario.obj", scenario);
			String scenarioName = scenario.getName();
			TestDataUtils.populateTestdata(scenarioName);
			testBase.getDriver().manage().deleteAllCookies();
			testBase.maximizeWindow();
			if (testBase.getString("sauce", "false").equalsIgnoreCase("false")) {
				System.out.println("Sauce User: " + TestDataUtils.getString("sauce.username"));
				System.out.println("Sauce Key: " + TestDataUtils.getString("sauce.access.key"));
				// SauceUtils.setJobName(scenarioName);
			}
		}
		session = testBase.getSessionID();
		System.out.println("Session: " + session);
		if (session.equalsIgnoreCase("") && !testBase.getContext().getString("sauce").equalsIgnoreCase("false")) {
			SauceREST sClient = new SauceREST(testBase.getContext().getString("sauce.username"),
					testBase.getContext().getString("sauce.access.key"));
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("name", scenario.getName());
			System.out.println(params.get("name"));
			sClient.updateJobInfo(session, params);
		}
	}

	@After
	public void afterHook(Scenario scenario) {
		synchronized (this) {
			WebDriver driver = TestBaseProvider.getTestBase().getDriver();
			if (scenario.isFailed()) {
				try {
					scenario.write("Current Page URL is " + driver.getCurrentUrl());
					byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
					scenario.embed(screenshot, "image/png");
				} catch (WebDriverException somePlatformsDontSupportScreenshots) {
					System.err.println(somePlatformsDontSupportScreenshots.getMessage());
				}
			}
			String session = TestBaseProvider.getTestBase().getSessionID();
			if (scenario.isFailed())
				System.out.println("ScenarioFailed = " + scenario.getName() + "<>Session=" + session
						+ "<>Status=Passed<>Platform = "
						+ TestBaseProvider.getTestBase().getContext().getString("driver.name"));
			else
				System.out.println("ScenarioFailed = " + scenario.getName() + "<>Session=" + session
						+ "<>Status=Failed<>Platform = "
						+ TestBaseProvider.getTestBase().getContext().getString("driver.name"));
			System.out.println("SauceOnDemandSessionID = " + session + " job-name=" + scenario.getName());
			System.out.println(TestBaseProvider.getTestBase().getDriver().manage().window().getSize());
			if (session.equalsIgnoreCase("")
					&& !TestBaseProvider.getTestBase().getContext().getString("sauce").equalsIgnoreCase("false")) {
				TestBase testBase = TestBaseProvider.getTestBase();
				SauceREST sClient = new SauceREST(testBase.getContext().getString("sauce.username"),
						testBase.getContext().getString("sauce.access.key"));
				System.out.println("SessionID: " + session);
				if (scenario.isFailed())
					sClient.jobFailed(session);
				else
					sClient.jobPassed(session);
			}
			TestBaseProvider.getTestBase().tearDown();
		}
	}
}