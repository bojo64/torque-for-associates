/* PLEASE READ
 * - This folder/file will hold the "steps" used for one feature in the in the test case as stated in the associated features file (with the annotations [e.g., @Given, @And, etc.)
 * - Usually creates an object reference to the "page" class(es) where most of the actions will take place
 * - User can have multiple steps defined in this file even if they have the same annotations
 */

package com.corpHomeTest.steps;

import org.openqa.selenium.WebElement;

import com.corpHomeTest.pages.CorpHomePage;
import com.corpHomeTest.utils.MyScholasticCommonUtil;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * @author Anish Ohri
 */

public class CorpHomePageSteps {
	
	MyScholasticCommonUtil mySchlUtil = new MyScholasticCommonUtil();
	CorpHomePage topNavBar = new CorpHomePage();

	@Given("^user is on the Scholastic Corporate homepage$")
	public void userLaunchApp() throws Throwable{
		topNavBar.openPage();
	}
	
	@When("^user hovers over the Shop dropdown$")
	public void user_hovers_over_dropdown() throws Throwable{
		topNavBar.hoverDropDown();
	}
	
	@When("^user clicks on the 'navButton' button$")
	public void user_clicks_on_button() throws Throwable{
		topNavBar.clickExpectedButton();
	}
	
	@When("^user enters their 'emailCredential' and 'passwordCredential' and sign-in$")
	public void user_logs_in() throws Throwable{
		topNavBar.sendLogInCredentials();
	}
	
	@When("^user navigates back one page$")
	public void user_does_not_log_in() throws Throwable{
		mySchlUtil.backOnePage();
	}
	
	@Then("^user should be brought to the 'pageTitle' expected webpage$")
	public void user_brought_to_expected_webpage() throws Throwable {
		topNavBar.verifyPageTitle();
	}
	
	@Then("^user should be brought back to the 'corporateHomeTitle'$")
	public void user_brought_to_corporate_home_webpage() throws Throwable {
		topNavBar.verifyCorporateHome();
	}
}
