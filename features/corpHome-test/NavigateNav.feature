Feature: Verify that user can go to all links

Scenario Outline: Navigate to relevant sites:[<UsingData>]
	Given user is on the Scholastic Corporate homepage
	When user clicks on the 'navButton' button
	Then user should be brought to the 'pageTitle' expected webpage
	
	Examples:
		|UsingData|
		|Book_Clubs|
		|Book_Fairs|
		|Educators|
		|Magazines|
		|Corp_Home|

Scenario Outline: Navigate to linked stores:[<UsingData>]
	Given user is on the Scholastic Corporate homepage
	When user hovers over the Shop dropdown
	When user clicks on the 'navButton' button
	Then user should be brought to the 'pageTitle' expected webpage
	
	Examples:
	|UsingData|
	|The_Teacher_Store|
	|The_Scholastic_Store|

Scenario Outline: Navigate to log-in page and log-in:[<UsingData>]
	Given user is on the Scholastic Corporate homepage
	When user clicks on the 'navButton' button
	Then user should be brought to the 'pageTitle' expected webpage
	When user enters their 'emailCredential' and 'passwordCredential' and sign-in
	Then user should be brought back to the 'corporateHomeTitle'
	
	Examples:
	|UsingData|
	|Complete_Login|
	
Scenario Outline: Navigate to log-in page and back to Corporate Home:[<UsingData>]
	Given user is on the Scholastic Corporate homepage
	When user clicks on the 'navButton' button
	Then user should be brought to the 'pageTitle' expected webpage
	When user navigates back one page
	Then user should be brought back to the 'corporateHomeTitle'
	
	Examples:
	|UsingData|
	|InComplete_Login|
	
	